<?php

class custom_links extends rcube_plugin {
    function init() {
        $this->load_config();

        $skin_path = $this->local_skin_path();
        $this->include_stylesheet("$skin_path/style.css");
        $this->include_stylesheet('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css');

        foreach (rcube::get_instance()->config->get('custom_links_taskbar', array()) as $index => $taskbar_link) {
            $content = $taskbar_link['label'];
            if (isset($taskbar_link['fontawesomeIcon'])) {
                $faIcon = $taskbar_link['fontawesomeIcon'];
                $content = "<i class=\"$faIcon\"></i> $content";
            }

            $this->add_button(array(
                'type' => 'link',
                /*'label' => array(
                    "name" => "custom_links_taskbar_" . $index,
                    "en_us" => $taskbar_link['label']
                ),*/
                'content' => $content,
                'href' => $taskbar_link['href'],
                'target' => $taskbar_link['target'],
                'class' => '',
                'classsel' => 'button-selected',
                'innerclass' => 'button-inner custom-link-button-inner',
            ), 'taskbar');
        }
    }
}
