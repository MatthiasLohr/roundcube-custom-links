# Custom Links - Roundcube Plugin

Roundcube plugin for adding custom links to your Roundcube instance.

## Installation

To use this plugin in your roundcube instance, you can use one of the following installation methods:

### Using composer

Using [composer](https://getcomposer.org/), you can install the plugin using the following command, which needs to be executing with the roundcube root directory:
```bash
composer require matthiaslohr/custom-links
```

For more information on the composer package, please check https://packagist.org/packages/matthiaslohr/custom-links.


### Manual Install

To manually install this plugin, please download the source code and extract it to `plugins/custom_links`.


## Configuration

The plugin can add an arbitrary number of additional links to the taskbar.
For this, the configuration variable `custom_links_taskbar` must be set to a list of associative arrays as shown below.
Following attributes can be set:

  * `label`: The label of the link to be displayed (will not be translated).
  * `href`: The target where the link points to.
  * `target`: The target frame of the link (`_blank` for a new window/tab, `_self` for the open in the current).
  * `fontawesomeIcon`: Add a [fontawesome icon](https://fontawesome.com/v5.15/icons). 


### Example configuration
```php
$config['custom_links_taskbar'] = array(array(
    "label" => "Visit mlohr.com",
    "href" => "https://mlohr.com/",
    "target" => "_blank",
    "fontawesomeIcon" => "fas fa-external-link-alt"
));
```


## License

This project is published under the [GNU GPLv3 License](LICENSE.md).

Copyright (c) 2021-2023 by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
