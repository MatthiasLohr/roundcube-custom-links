# Changelog

## v1.0.0

**This is a new major release with breaking changes!**
Please update all files and check your settings!

  * Clearified plugin name: `custom_links` it is
  * Fixed example configuration in README (#3, #4)

## v0.1.2

  * Fixed usage of configuration variables: assoziative arrays instead of object

## v0.1.1

  * Fixed wrong icon placement with elastic skin (#1, thx to @joris.dedieu for reporting)

## v0.1.0

  * First implementation
